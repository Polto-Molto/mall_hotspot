// Inspired from : https://codepen.io/suez/pen/dPqxoM


var setEmailInterest = function(email,phone,interest)
{
	data = "email=" + email + "&" + "phone=" + phone + "&" + "interest=" + interest;
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
               alert(this.responseText);
            }
        };
        xmlhttp.open("GET","setUserInterestings.php?"+data,true);
        xmlhttp.send();

}

var checkNewUser = function(callback)
{
var user_mac = "";
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                 user_mac = this.responseText.toString();
		callback( user_mac);
            }
        };
        xmlhttp.open("GET","check_new_user.php",true);
        xmlhttp.send();
}


$(document).ready(function() {
var user_mac = "";

    var animating = false,
        firstCheck = true,
        $spinner = $(".spinner"),
        $login = $(".login"),
        $connected = $(".connected"),
        $login__submit = $(".login__submit");

    chilliController.host = "192.168.10.1";
    chilliController.port = "3990";
    chilliController.interval = 60;

    chilliController.onError = handleErrors;
    chilliController.onUpdate = updateUI;


/*    checkNewUser(function(mac){
      user_mac = mac;
      if(user_mac.length > 1)
      {
        var username =  document.getElementById('login__username').value ;
        var password =  document.getElementById('login__password').value ;
        var email    =  document.getElementById('login_email').value;
        var interest =  document.getElementById('interest').value;
        var phone =  document.getElementById('phone_number').value;
        if (username == null || username == '' || password == null || password == '') {
            document.getElementById('login__errors').innerHTML =
                "<div class='login__row'>" + "<p class='login__signup login__error'>" +
                "Username and password required" +"</p>" +"</div>";
           return;
        }
        setTimeout(function() {
            connect(username, password);
        }, 100);
      }
    });
*/
    function handleErrors (code) {
	window.location = "https://www.destinyusa.com/sales/";
    /*    if (animating) {
            animating = false;
            $login__submit.removeClass("processing");
        }*/
    }

    function ripple(elem, e) {
        $(".ripple").remove();
        var elTop = elem.offset().top,
            elLeft = elem.offset().left,
            x = e.pageX - elLeft,
            y = e.pageY - elTop;
        var $ripple = $("<div class='ripple'></div>");
        $ripple.css({top: y, left: x});
        elem.append($ripple);
    };

    function connect(username, password) {
        chilliController.logon( username , password ) ;
    }

    function updateUI() {
        if (firstCheck && chilliController.clientState == 1) {
            // First call and user is already logged in
            $spinner.hide();
            $connected.show();
        } else if (!firstCheck && chilliController.clientState == 1) {
            // Not first call and user is now logged in
            location.reload();
        } else if (chilliController.clientState == 0 && chilliController.command === 'logon') {
            // User not logged in with error message
            document.getElementById('login__errors').innerHTML = "<div class='login__row'>" +
                "<p class='login__signup login__error'>" +
                "Username or password incorrect" +
                "</p>" +
                "</div>";

            animating = false;
            $login__submit.removeClass("processing");
        } else {
            // First check, so display login form
            firstCheck = false;
            $spinner.hide();
            $login.show();
   }
}


      $(document).on("click", ".login__submit", function(e) {
        if (animating) return;
        var username =  document.getElementById('login__username').value ;
        var password =  document.getElementById('login__password').value ;
        var email    =  document.getElementById('login_email').value;
        var interest =  document.getElementById('interest').value;
        var phone =  document.getElementById('phone_number').value;
        if (username == null || username == '' || password == null || password == '') {
            document.getElementById('login__errors').innerHTML = 
		"<div class='login__row'>" + "<p class='login__signup login__error'>" +
                "Username and password required" +"</p>" +"</div>";
           return;
        }
        animating = true;
        ripple($login__submit, e);
        $login__submit.addClass("processing");
        setTimeout(function() {
            connect(username, password);
       	    setEmailInterest(email,phone,interest);
        }, 100);
    });

    $spinner.show();
    chilliController.refresh();
});

