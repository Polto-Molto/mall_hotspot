Preparing Raspbian OS : 
------------------------

cd /home/pi

sudo apt-get update

sudo apt-get install -y curl git

curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -

sudo apt-get update

sudo apt-get install -y nodejs

sudo apt-get install -y sendmail

sudo npm install pm2 -g


Preparing Reboot Action :
------------------------

nano /etc/sudoers

set these lines

root	ALL=(ALL:ALL) ALL

www-data ALL=NOPASSWD: /sbin/reboot

%sudo	ALL=(ALL:ALL) ALL

%www-data ALL=NOPASSWD: /sbin/reboot


Installing Hotspot Software :
------------------------------

goto home directory /home/pi

run:

git clone https://github.com/pihomeserver/Kupiki-Hotspot-Script.git

cd Kupiki-Hotspot-Script

sudo chmod +x pihotspot.sh && sudo ./pihotspot.sh

reboot



Installing Mall Software :
---------------------------

goto home directory /home/pi

run: 

git clone https://Polto-Molto@bitbucket.org/Polto-Molto/mall_hotspot.git

cd mall_hotspot

sudo cp -PR ./tools/*       /usr/sbin

sudo cp -PR ./html/*       /usr/share/nginx/html

sudo cp -PR ./portal/*     /usr/share/nginx/portal

sudo cp -PR ./package      /home/pi



Installing Help tools :
------------------------

goto home directory /home/pi

run: 

cd mall_hotspot

sudo cp -PR ./help_tools/* /usr/sbin/



Installing Database :
----------------------

goto home directory /home/pi

run:


mysql -u root -p < ./package/radius.sql

note: this will ask for password , it is "pihotspot"



Insalling Autoboot Script :
----------------------------

goto home directory /home/pi

run: 

cd mall_hotspot

sudo cp -PR ./etc/* /etc

reboot

Installing Done ....


Usage :
--------

After Installing Done :

you can connect to Raspberry pi to set settings from any pc on the same network:

from any browser:

http://pihotspot.local/setting.html

set your settings and set password "sgetty"

click save < this will save data abd auto reboot ....

then you can simply connect to pihotspot network to connect to net and set 
interestings


