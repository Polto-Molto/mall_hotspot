-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 25, 2017 at 11:16 AM
-- Server version: 5.5.55-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `radius`
--
USE radius;

-- --------------------------------------------------------

--
-- Table structure for table `hotspot_users_deals`
--

CREATE TABLE IF NOT EXISTS `hotspot_users_deals` (
  `title` varchar(256) NOT NULL,
  `permalink` varchar(256) NOT NULL,
  `hide_sale_date` date NOT NULL,
  `tenant` varchar(50) NOT NULL,
  `content` varchar(512) NOT NULL,
  `image` varchar(256) NOT NULL,
  `tags` varchar(512) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotspot_users_interesting`
--

CREATE TABLE IF NOT EXISTS `hotspot_users_interesting` (
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(80) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `interesting` varchar(512) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `mac` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hotspot_users_deals`
--
ALTER TABLE `hotspot_users_deals`
 ADD PRIMARY KEY (`title`);

--
-- Indexes for table `hotspot_users_interesting`
--
ALTER TABLE `hotspot_users_interesting`
 ADD UNIQUE KEY `mac` (`mac`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
