var async = require('async');
var request = require('request');
var nodemailer = require('nodemailer');
var mysql = require('mysql');

var twilio = require('twilio');
var accountSid = 'AC6e3c65830998e330b773d4a690c04633'; // Your Account SID from$
var authToken = '0a5b7edc23debaa5c6d57b71e296768a';   // Your Auth Token from w$

var twilio = require('twilio');
var client = new twilio(accountSid, authToken);


var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'textrping@gmail.com',
    pass: 'raspberry'
  }
});

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'radius',
  password : 'radpass',
  database : 'radius'
});

var exec = require("child_process").exec;

var settings = require("../settings.json");


var Deals;
var chilli_users = [];
var OnlineUsers;
var sms_enabled = settings.sms_enabled;
var sending_deals = [] ;

console.log("Connecting Database ...");
connection.connect();
console.log("Database Connected ...");

var exec_str = "sudo chilli --lease="+settings.client_allocate_time+" --leaseplus="+settings.client_removal_time;

exec("sudo service chilli stop ; sudo killall chilli", function (error, stdout, stderr) {

  exec(exec_str, function (error, stdout, stderr) {
    console.log(exec_str)
    console.log(error|stdout)
  })
})

var sendDeals = function(chilli_users_array,callback){
  console.log("Get Emails of Connected Devices ...");
  chilli_users_array.forEach(function(chilli_user) {
    connection.query('SELECT *,TIME_TO_SEC(TIMEDIFF(NOW(),datetime)) as diff FROM hotspot_users_interesting WHERE mac = ?',[chilli_user.mac], function (error, result, fields){
    if(result.length > 0)
    {
      if(result[0].diff >= settings.user_deals_period)
      {	
        connection.query('UPDATE hotspot_users_interesting SET datetime = NOW() WHERE mac = ?',chilli_user.mac, function (error, update_result, fields){
      	console.log("Found Online User Email : " + result[0].email);
      	console.log("Check User Email : " + result[0].email + " For Current "+ Deals.length +" Deals ...");
      	checkDealsForUser(result[0]);
	})
      }
      else
	console.log("USER : "+ result[0].email +" TAKE DEALS FROM " + result[0].diff + " SECONDS WHICH IS LESS THAN 1 day ... ");
    }
   });
  })
}

var checkDealsForUser = function(user,callback)
{
  var itemsProcessed = 0;
  var email_text = "";  
  var sms_text = "";
  var interestings = user.interesting.split(":");
  sending_deals = []
    async.forEachSeries(interestings, function(interest, callback_1) {

      async.forEachSeries(Deals, function(deal, callback_2) {

	if(deal.tags.indexOf(interest) > -1 )
      	{
	  if( sending_deals.indexOf(deal.permalink) < 0)
	  { 
		email_text += "<h4>" + deal.tenant + " : " + deal.title +"</h4><br>" + 
			  "<a href='" +deal.permalink+ "' >" + "<img src='"+ deal.image + "'/>"+"</a><br>";
		sms_text += deal.tenant + " : " + deal.permalink + " " ;
		sending_deals.push(deal.permalink)
	        callback_2()
	  }
	  else
	    callback_2()
        }
	else
	  callback_2()

      }, function(err) {
	callback_1()
      });

    }, function(err) {
        if(email_text.length > 0 ) {
             console.log("Send EMail to : " + user.email);
             sendEmail( user.email , 'Deal from Mall',email_text);
        }
	else
	{
	    console.log("no deals found")
	}

        if(sms_text.length > 0 && sms_enabled == 1) 
        {
          console.log("Send sms :", sms_text);
          console.log("Send sms to : " + user.phone);
	  client.messages.create(
          {
            to: '+' + user.phone,
            from: '+14703497313',
            body: sms_text 
          },
          (err, message) => {
             console.log(message || err);
          });
        }
    });
}


var getDeals = function(callback){
  console.log("Collecting Deals from Mall Website ....");
  request('https://www.destinyusa.com/sales/', function (error, response, body) {
    var i = body.search("var localized ");
    var part_1 = body.slice(i+16, body.length);
    i = part_1.search("]");
    var part_2 = part_1.slice(0,i+2);
    var objects = JSON.parse(part_2);
    objects.sales.forEach(function(deal){
      var parts = deal.start_date.split('/');
      deal.start_date = new Date(parts[2],parts[0]-1,parts[1]);
      parts = deal.end_date.split('/');
      deal.end_date = new Date(parts[2],parts[0]-1,parts[1]);
//      connection.query('INSERT INTO hotspot_users_deals SET ?',deal, function (error, results, fields){
//      })
    })
    callback(objects.sales);
  })
}

var sendEmail = function(_to ,_subject ,_deal){
  var mailOptions = {
    from: 'ahbanna2007@gmail.com',
    to: _to,
    subject: _subject,
    html: "<html><body><center>" +_deal +"</center></body></html>"
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}

getDeals(function(results){
  Deals = results;
})


setInterval( function(){
  getDeals(function(results){
    Deals = results;
  })
}, settings.update_deals_period * 1000);


var getChilliUsers = function(callback){
  var chilli_users = []
  exec("chilli_query list", function (error, stdout, stderr) {
    var lines = stdout.split("\n")
    var check_finished = 0;
    lines.pop()
    lines.forEach(function(line){
      var data = line.split(" ")
      var chilli_user = new Object();
      chilli_user.mac = data[0]
      chilli_user.ip  = data[1]
      chilli_user.status = data[2]
      chilli_user.date = data[3].substr(0,10)
      chilli_user.id = data[3].substr(10,8)
      chilli_user.time_from_login = data[6].split("/")[0]
      chilli_user.time_online = data[7].split("/")[0]

      if(chilli_user.time_online <= 100)
      {
         chilli_users.push(chilli_user)
	 check_finished++
      }
      else
	check_finished++

      if(check_finished == lines.length)
        callback(chilli_users)
    })
  })
}


setInterval( function(){
 getChilliUsers(function(ch_users){
  sendDeals(ch_users,function(){
  })
 })
}, settings.check_connected_clients_time * 1000);

