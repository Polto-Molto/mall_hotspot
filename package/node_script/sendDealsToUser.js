var async = require('async');
var request = require('request');
var nodemailer = require('nodemailer');
var mysql = require('mysql');

var twilio = require('twilio');
var accountSid = 'AC6e3c65830998e330b773d4a690c04633'; // Your Account SID from$
var authToken = '0a5b7edc23debaa5c6d57b71e296768a';   // Your Auth Token from w$

var twilio = require('twilio');
var client = new twilio(accountSid, authToken);


var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'textrping@gmail.com',
    pass: 'raspberry'
  }
});

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'radius',
  password : 'radpass',
  database : 'radius'
});

var Deals;
var OnlineUsers;
var sms_enabled = 0;
var sending_deals = [] ;

console.log("Connecting Database ...");
connection.connect();
console.log("Database Connected ...");


var sendDeals = function(callback){
  console.log("Get Emails of Connected Devices ...");
    connection.query('SELECT * FROM hotspot_users_interesting WHERE mac = ?',[process.argv[2]], function (error, result, fields){
    if(result.length > 0)
    {
      	console.log("Found Online User Email : " + result[0].email);
      	console.log("Check User Email : " + result[0].email + " For Current Deals ...");
      	checkDealsForUser(result[0]);
    }
    connection.end()
  });
}

var checkDealsForUser = function(user,callback)
{
  var itemsProcessed = 0;
  var email_text = "";  
  var sms_text = "";
  var interestings = user.interesting.split(":");

    async.forEachSeries(interestings, function(interest, callback_1) {

      async.forEachSeries(Deals, function(deal, callback_2) {

	if(deal.tags.indexOf(interest) > -1 )
      	{
	  if( sending_deals.indexOf(deal.permalink) < 0)
	  { 
		email_text += "<h4>" + deal.tenant + " : " + deal.title +"</h4><br>" + 
			  "<a href='" +deal.permalink+ "' >" + "<img src='"+ deal.image + "'/>"+"</a><br>";
		sms_text += deal.tenant + " : " + deal.permalink + " " ;
		sending_deals.push(deal.permalink)
	        callback_2()
	  }
	  else
	    callback_2()
        }
	else
	  callback_2()

      }, function(err) {
	callback_1()
      });

    }, function(err) {
        if(email_text.length > 0 ) {
             console.log("Send EMail to : " + user.email);
             sendEmail( user.email , 'Deal from Mall',email_text);
        }
        if(sms_text.length > 0 && sms_enabled == 1) 
        {
          console.log("Send sms :", sms_text);
          console.log("Send sms to : " + user.phone);
	  client.messages.create(
          {
            to: '+' + user.phone,
            from: '+14703497313',
            body: sms_text 
      //    mediaUrl: 'https://c1.staticflickr.com/3/2899/14341091933_1e92e62d12_b.jp$
          },
          (err, message) => {
             console.log(message || err);
          });
        }
    });
}


var getDeals = function(callback){
  console.log("Collecting Deals from Mall Website ....");
  request('https://www.destinyusa.com/sales/', function (error, response, body) {
    var i = body.search("var localized ");
    var part_1 = body.slice(i+16, body.length);
    i = part_1.search("]");
    var part_2 = part_1.slice(0,i+2);
    var objects = JSON.parse(part_2);
    objects.sales.forEach(function(deal){
      var parts = deal.start_date.split('/');
      deal.start_date = new Date(parts[2],parts[0]-1,parts[1]);
      parts = deal.end_date.split('/');
      deal.end_date = new Date(parts[2],parts[0]-1,parts[1]);
      connection.query('INSERT INTO hotspot_users_deals SET ?',deal, function (error, results, fields){
      })
    })
    callback(objects.sales);
  })
}

var sendEmail = function(_to ,_subject ,_deal){
  var mailOptions = {
    from: 'ahbanna2007@gmail.com',
    to: _to,
    subject: _subject,
    html: "<html><body><center>" +_deal +"</center></body></html>"
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}


getDeals(function(results){
  Deals = results;
//console.log(Deals)
  if(Deals.length > 0)
    sendDeals(function(str){
    })
})

